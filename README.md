```bash
nix develop
flash
```

Setup udev rules, add this to your `configuration.nix`:
```nix
services.udev.packages = [
  (pkgs.writeTextFile {
    name = "probe-rs udev rules";
    text = ''ATTRS{idVendor}=="0483", ATTRS{idProduct}=="3748", MODE="660", GROUP="users", TAG+="uaccess"'';
    destination = "/etc/udev/rules.d/69-probe-rs.rules";
  })
];
```
