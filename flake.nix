{
  description = "twiceaday";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, fenix, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ fenix.overlays.default ];
        };

        # derivation for tools like cargo, rustc, clippy, etc
        rstoolchain = (pkgs.fenix.fromToolchainFile {
          file = ./rust-toolchain.toml;
          sha256 = "sha256-SXRtAuO4IqNOQq+nLbrsDFbVk+3aVA8NNpSZsKlVH/8=";
        });

        flashscript = pkgs.writeShellScriptBin "flash" ''
          ${rstoolchain}/bin/cargo embed --release
        '';

        checkscript = pkgs.writeShellScriptBin "check" ''
          ${rstoolchain}/bin/cargo-clippy check -- --allow clippy::upper_case_acronyms --deny warnings
        '';
      in
      {
        devShells.default = pkgs.mkShell {
          packages = with pkgs; [
            probe-rs
            rstoolchain
            flashscript
            checkscript
            (python311.withPackages (ps: with ps; [ numpy pillow ]))
          ];
        };
      });
}
