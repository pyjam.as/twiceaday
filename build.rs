use std::time::SystemTime;

fn main() {
    let now = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_millis();
    let danish_offset = 60 * 60 * 1000; // 👌
    println!("cargo:rustc-env=CURRENT_TIME={}", now + danish_offset);
}
