import json
import string

import numpy as np
from PIL import ImageFont, Image, ImageDraw

np.set_printoptions(edgeitems=300, linewidth=100000)


fonts = {
    # name: (size, spacing, path)
    "Large": (32, 4, "res/FiraMono-Bold.ttf"),
    "Medium": (20, 3, "res/FiraMono-Regular.ttf"),
    # no need for the small font at the moment - save space instead
    # "Small": (16, 1, "res/FiraMono-Regular.ttf"),
}
characters = string.ascii_letters + string.digits + "? :.-ø"

template_fonts = ""
template_shapes = ""
template_spacing = ""
template_bitmaps = ""
for font_name, (size_px, font_spacing, font_path) in fonts.items():
    font = ImageFont.truetype(font=font_path, size=size_px)

    # https://pillow.readthedocs.io/en/stable/handbook/text-anchors.html
    anchor = "ma"
    boxes = [font.getbbox(c, anchor=anchor) for c in characters]
    size_x = max(right for left, top, right, bottom in boxes) - min(left for left, top, right, bottom in boxes)
    size_y = max(bottom for left, top, right, bottom in boxes)

    def char2array(c) -> np.ndarray:
        img = Image.new(mode="L", size=(size_x, size_y))
        d = ImageDraw.Draw(img)
        d.text((size_x/2, 0), c, fill="white", anchor=anchor, font=font)
        return np.array(img)

    # print(char2array("Q"))
    # print(char2array("j"))
    # print(char2array("a"))

    arrays = {
        c: char2array(c)
        for c in characters
    }

    lists = {
        c: [int(x) for x in a.ravel()]
        for c, a in arrays.items()
    }
    assert len({len(l) for l in lists.values()}) == 1

    lists_json = {
        c: json.dumps(l)
        for c, l in lists.items()
    }

    maps = [
        f"        ('{c}', Font::{font_name}) => &{j},"
        for c, j in lists_json.items()
    ]

    template_fonts += f"    {font_name},\n"
    template_shapes += f"            Font::{font_name} => ({size_x}, {size_y}),\n"
    template_spacing += f"            Font::{font_name} => {font_spacing},\n"
    template_bitmaps += "\n".join(maps)
    template_bitmaps += f"        (_, Font::{font_name}) => &{lists_json['?']},"


rust = f"""
pub enum Font {{
{template_fonts}
}}

impl Font {{
    pub fn get_shape(&self) -> (usize, usize) {{
        match self {{
{template_shapes}
        }}
    }}

    pub fn get_size(&self) -> usize {{
        let (x, y) = self.get_shape();
        return x * y;
    }}

    pub fn get_spacing(&self) -> usize {{
        match self {{
{template_spacing}
        }}
    }}
}}

pub fn get_bitmap(c: char, f: &Font) -> &'static [u8] {{
    match (c, f) {{
{template_bitmaps}
    }}
}}
"""
print(rust)
