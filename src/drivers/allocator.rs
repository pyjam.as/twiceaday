// Bump pointer allocator implementation
// From https://docs.rust-embedded.org/book/collections/

use core::alloc::{GlobalAlloc, Layout};
use core::cell::UnsafeCell;
use core::ptr;
use cortex_m::interrupt;
use rtt_target::rprintln;

// Bump pointer allocator for *single* core systems
struct BumpPointerAlloc {
    head: UnsafeCell<usize>,
    end: usize,
}

unsafe impl Sync for BumpPointerAlloc {}

unsafe impl GlobalAlloc for BumpPointerAlloc {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        // `interrupt::free` is a critical section that makes our allocator safe
        // to use from within interrupts
        interrupt::free(|_| {
            let head = self.head.get();
            let size = layout.size();
            let align = layout.align();
            let align_mask = !(align - 1);

            // move start up to the next alignment boundary
            let start = (*head + align - 1) & align_mask;

            if start + size > self.end {
                // a null pointer signal an Out Of Memory condition
                rprintln!("Out of Memory 😥");
                ptr::null_mut()
            } else {
                *head = start + size;
                start as *mut u8
            }
        })
    }

    unsafe fn dealloc(&self, _: *mut u8, _: Layout) {
        // this allocator never deallocates memory
        rprintln!("leaked memory 🙃");
    }
}

// Declaration of the global memory allocator
// NOTE: the user must ensure that the memory regions are not used by other
// parts of the program.
// From https://infocenter.nordicsemi.com/pdf/nRF52832_PS_v1.4.pdf section 8,
// we are using RAM{1,7}, but *not* RAM0, as this is used by RTT.
#[global_allocator]
static HEAP: BumpPointerAlloc = BumpPointerAlloc {
    head: UnsafeCell::new(0x2000_2000),
    end: 0x2000_F000,
};

// TODO: Handle Out Of Memory (OOM) errors 🤷
// See https://docs.rust-embedded.org/book/collections/
