use crate::hal::gpio::{
    p0::{P0_13, P0_15},
    Disconnected, Floating, Input, Level, Output, PushPull,
};
use embedded_hal::digital::v2::{InputPin, OutputPin};
use embedded_hal::prelude::_embedded_hal_blocking_delay_DelayUs;
use hal::{pac::TIMER0, Timer};
use nrf52832_hal as hal;

pub struct Button {
    enable_pin: P0_15<Output<PushPull>>,
    state_pin: P0_13<Input<Floating>>,
}

impl Button {
    pub fn new(p13: P0_13<Disconnected>, p15: P0_15<Disconnected>) -> Self {
        Button {
            enable_pin: p15.into_push_pull_output(Level::Low),
            state_pin: p13.into_floating_input(),
        }
    }

    pub fn pressed(&mut self, delay: &mut Timer<TIMER0>) -> bool {
        /*
        From the wiki:

        > Note that the button consumes around 34µA when P0.15 is left high.
        > To reduce current consumption, set it to low most of the time and
        > only set it to high shortly before reading it. The button needs a
        > short time to give good outputs though, setting P0.15 high at least
        > four times in a row seems to result in enough delay that P0.13 has a
        > stable output.

        I am not sure, if this is how it should be done in rust, but leaving it
        here for now.
        */
        self.enable_pin.set_high().unwrap();

        delay.delay_us(100u16);

        let result = self.state_pin.is_high().unwrap();

        self.enable_pin.set_low().unwrap();

        result
    }
}
