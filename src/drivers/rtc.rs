use crate::hal::{pac::RTC0, Rtc};

pub struct RTCDriver {
    rtc: Rtc<RTC0>,
}

// OVERFLOWS EVERY 25 DAYS.
// IF YOU ENCOUTER THIS PROBLEM, YOU ARE THE
// LUCKY PERSON RESPONSIBLE FOR FIXING IT :)
impl RTCDriver {
    pub fn new(rtc0: RTC0) -> Self {
        // LFCLK = 32768 Hz
        // To get a clock of 8hz we divide LFCLK by 4095
        // 32768 Hz / (4095 + 1 Hz) = 8Hz
        let prescaler = 4095;
        let rtc = Rtc::new(rtc0, prescaler).unwrap();

        RTCDriver { rtc }
    }

    pub fn init(&mut self) {
        // LFCLK must be started before the RTC works.
        self.rtc.enable_counter();
    }

    pub fn uptime(&self) -> u32 {
        // Clock ticks at 8Hz so to get
        // seconds we divide by eight
        self.rtc.get_counter() / 8
    }
}
