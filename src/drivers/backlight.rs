use crate::hal::gpio::{
    p0::{P0_14, P0_22, P0_23},
    Disconnected, Level, Output, PushPull,
};
use crate::OutputPin;

#[allow(dead_code)]
pub enum Brightness {
    OFF,
    LOW,
    MID,
    HIGH,
}

pub struct Backlight {
    low: P0_14<Output<PushPull>>,
    mid: P0_22<Output<PushPull>>,
    high: P0_23<Output<PushPull>>,
}

impl Backlight {
    pub fn new(
        p14: P0_14<Disconnected>,
        p22: P0_22<Disconnected>,
        p23: P0_23<Disconnected>,
    ) -> Self {
        Backlight {
            low: p14.into_push_pull_output(Level::Low),
            mid: p22.into_push_pull_output(Level::High),
            high: p23.into_push_pull_output(Level::High),
        }
    }

    pub fn set(&mut self, brightness: Brightness) {
        match brightness {
            Brightness::OFF => {
                self.low.set_high().unwrap();
                self.mid.set_high().unwrap();
                self.high.set_high().unwrap();
            }
            Brightness::LOW => {
                self.low.set_low().unwrap();
                self.mid.set_high().unwrap();
                self.high.set_high().unwrap();
            }
            Brightness::MID => {
                self.low.set_high().unwrap();
                self.mid.set_low().unwrap();
                self.high.set_high().unwrap();
            }
            Brightness::HIGH => {
                self.low.set_high().unwrap();
                self.mid.set_high().unwrap();
                self.high.set_low().unwrap();
            }
        };
    }
}
