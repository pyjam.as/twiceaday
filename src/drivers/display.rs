use crate::hal::gpio::{
    p0::{P0_02, P0_03, P0_18, P0_25, P0_26},
    Disconnected, Level, Output, Pin, PushPull,
};
use core::cmp;
use embedded_hal::digital::v2::OutputPin;
use hal::{
    pac::{SPIM0, TIMER0},
    spim::{self, Spim},
    Timer,
};
use nrf52832_hal as hal;

use crate::DelayMs;

#[allow(dead_code)]
#[repr(u8)]
enum Instruction {
    NOP = 0x00,
    SWRESET = 0x01,
    RDDID = 0x04,
    RDDST = 0x09,
    SLPIN = 0x10,
    SLPOUT = 0x11,
    PTLON = 0x12,
    NORON = 0x13,
    INVOFF = 0x20,
    INVON = 0x21,
    DISPOFF = 0x28,
    DISPON = 0x29,
    CASET = 0x2A,
    RASET = 0x2B,
    RAMWR = 0x2C,
    RAMRD = 0x2E,
    PTLAR = 0x30,
    VSCRDER = 0x33,
    TEOFF = 0x34,
    TEON = 0x35,
    MADCTL = 0x36,
    VSCAD = 0x37,
    COLMOD = 0x3A,
    VCMOFSET = 0xC5,
}

pub struct Display {
    spi: Spim<SPIM0>,
    dc: Pin<Output<PushPull>>,
    cs: Pin<Output<PushPull>>,
    rst: Pin<Output<PushPull>>,
}

impl Display {
    pub fn new(
        spim: SPIM0,
        p2: P0_02<Disconnected>,
        p3: P0_03<Disconnected>,
        p18: P0_18<Disconnected>,
        p25: P0_25<Disconnected>,
        p26: P0_26<Disconnected>,
    ) -> Self {
        let spi_clk = p2.into_push_pull_output(Level::Low).degrade();
        let spi_mosi = p3.into_push_pull_output(Level::Low).degrade();
        let spi_pins = spim::Pins {
            sck: Some(spi_clk),
            miso: None,
            mosi: Some(spi_mosi),
        };
        let display_spi = Spim::new(spim, spi_pins, spim::Frequency::M8, spim::MODE_3, 0);

        Display {
            spi: display_spi,
            cs: p25.into_push_pull_output(Level::Low).degrade(),
            dc: p18.into_push_pull_output(Level::Low).degrade(),
            rst: p26.into_push_pull_output(Level::Low).degrade(),
        }
    }

    pub fn init(&mut self, delay: &mut Timer<TIMER0>) {
        // Hard reset display
        self.rst.set_low().unwrap();
        delay.delay_ms(10u16);
        self.rst.set_high().unwrap();
        delay.delay_ms(10u16);

        self.send_command(Instruction::SWRESET);
        delay.delay_ms(150u16);
        self.send_command(Instruction::SLPOUT);
        delay.delay_ms(10u16);
        self.send_command(Instruction::INVOFF);
        delay.delay_ms(10u16);
        self.send_command(Instruction::VSCRDER);
        self.send_data(&[0u8, 0u8, 0x14u8, 0u8, 0u8, 0u8]);
        delay.delay_ms(10u16);
        self.send_command(Instruction::MADCTL);
        self.send_data(&0b00000000u8.to_be_bytes());
        delay.delay_ms(10u16);
        self.send_command(Instruction::COLMOD);
        self.send_data(&0b01010101u8.to_be_bytes());
        delay.delay_ms(10u16);
        self.send_command(Instruction::INVON);
        delay.delay_ms(10u16);
        self.send_command(Instruction::NORON);
        delay.delay_ms(10u16);
        self.send_command(Instruction::DISPON);
        delay.delay_ms(10u16);
    }

    #[allow(dead_code)]
    pub fn write_fill(&mut self, xs: u16, xe: u16, ys: u16, ye: u16, color: u16) {
        self.set_bounds(xs, xe, ys, ye);
        self.send_command(Instruction::RAMWR);
        let mut buf = [0u8; 240 * 2];
        for i in 0..(xe - xs) as usize {
            buf[i * 2] = color.to_be_bytes()[0];
            buf[i * 2 + 1] = color.to_be_bytes()[1];
        }
        for _ in 0u16..ye - ys {
            self.send_data(&buf[0..((ye - ys) * 2) as usize]);
        }
    }

    pub fn write_area(&mut self, xs: u16, xe: u16, ys: u16, ye: u16, buf16: &[u16]) {
        self.set_bounds(xs, xe, ys, ye);
        self.send_command(Instruction::RAMWR);
        let mut written: usize = 0;
        let buf16_size: usize = ((xe - xs) * (ye - ys)) as usize;
        while written < buf16_size {
            let mut buf8 = [0u8; 240 * 2];
            let chunk_size: usize = cmp::min(buf16_size - written, 240);
            for i in 0..chunk_size {
                buf8[i * 2] = buf16[written + i].to_be_bytes()[0];
                buf8[i * 2 + 1] = buf16[written + i].to_be_bytes()[1];
            }
            self.send_data(&buf8[0..(chunk_size * 2)]);
            written += chunk_size;
        }
    }

    #[allow(dead_code)]
    pub fn write_pixel(&mut self, x: u16, y: u16, color: u16) {
        self.set_bounds(x, x, y, y);
        self.send_command(Instruction::RAMWR);
        self.send_data(&color.to_be_bytes());
    }

    fn set_bounds(&mut self, xs: u16, xe: u16, ys: u16, ye: u16) {
        self.send_command(Instruction::CASET);
        self.send_data(&xs.to_be_bytes());
        self.send_data(&(xe - 1).to_be_bytes());
        self.send_command(Instruction::RASET);
        self.send_data(&ys.to_be_bytes());
        self.send_data(&(ye - 1).to_be_bytes());
    }

    fn send_command(&mut self, cmd: Instruction) {
        self.dc.set_low().unwrap();
        let _ = self.spi.write(&mut self.cs, &[cmd as u8]);
    }

    fn send_data(&mut self, data: &[u8]) {
        self.dc.set_high().unwrap();
        let _ = self.spi.write(&mut self.cs, data);
    }
}
