use crate::Display;

use crate::font::{get_bitmap, Font};
use num_traits::Float;


pub const WIDTH: i16 = 240;
pub const HEIGHT: i16 = 240;

pub struct Graphics {
    display: Display,
    color: u16,
    stroke: f32,
}

impl Graphics {
    pub fn new(display: Display) -> Self {
        Graphics {
            display,
            color: 0,
            stroke: 3.0,
        }
    }

    pub fn set_color(&mut self, r: u8, g: u8, b: u8) {
        self.color = self.pack_rgb([
            ((r as f32 / 255.0) * 31.0) as u16,
            ((g as f32 / 255.0) * 63.0) as u16,
            ((b as f32 / 255.0) * 31.0) as u16,
        ]);
    }

    pub fn set_stroke(&mut self, stroke: f32) {
        // stroke width
        self.stroke = stroke;
    }

    pub fn clear(&mut self) {
        self.display.write_fill(0, 240, 0, 240, self.color);
    }

    #[inline(always)]
    fn pack_rgb(&mut self, rgb: [u16; 3]) -> u16 {
        (rgb[0] << 11) | (rgb[1] << 5) | rgb[2]
    }

    #[inline(always)]
    fn unpack_rgb(&mut self, color: u16) -> [u16; 3] {
        [
            (0b1111100000000000u16 & color) >> 11,
            (0b0000011111100000u16 & color) >> 5,
            0b0000000000011111u16 & color,
        ]
    }

    fn color_opacity(&mut self, color: u16, opacity: f32) -> u16 {
        let mut rgb = self.unpack_rgb(color);
        rgb[0] = (rgb[0] as f32 * opacity) as u16;
        rgb[1] = (rgb[1] as f32 * opacity) as u16;
        rgb[2] = (rgb[2] as f32 * opacity) as u16;
        self.pack_rgb(rgb)
    }

    pub fn render_text(&mut self, x: u16, y: u16, font: Font, text: &[char], color: u16) {
        let mut cursor = x;
        let mut buf = [0u16; 836];
        let (font_x, font_y) = font.get_shape();
        for character in text {
            let char_buf = get_bitmap(*character, &font);
            for i in 0..font.get_size() {
                let opacity = char_buf[i] as f32 / 255f32;
                buf[i] = self.color_opacity(color, opacity);
            }
            self.display.write_area(
                cursor,
                cursor + (font_x as u16),
                y,
                y + (font_y as u16),
                &buf,
            );
            cursor = cursor + (font_x as u16) + (font.get_spacing() as u16);
        }
    }

    pub fn draw_image(&mut self, x0: u8, y0: u8, x1: u8, y1: u8, img: &[u16]) {
        self.display
            .write_area(x0.into(), y0.into(), x1.into(), y1.into(), img);
    }

    fn the_math_checks_out_please_write_pixel(&mut self, x: i16, y: i16) {
        if !(0..=WIDTH).contains(&x) || !(0..=HEIGHT).contains(&y) {
            return;
        }

        for i in x - self.stroke as i16..x + self.stroke as i16 {
            for j in y - self.stroke as i16..y + self.stroke as i16 {
                let dist =
                    (((i - x).abs().pow(2) + (j - y).abs().pow(2)) as f32).sqrt() / self.stroke;

                if dist < 0.5 {
                    self.display
                        .write_pixel(i.unsigned_abs(), j.unsigned_abs(), self.color);
                }
            }
        }
    }

    #[allow(dead_code)]
    fn write_pixel_aa(&mut self, x: i16, y: i16, _alpha: f32) {
        // TODO
        // Get color
        // Blend
        self.display
            .write_pixel(x.unsigned_abs(), y.unsigned_abs(), self.color);
    }

    pub fn draw_straight_line(&mut self, x0: u8, y0: u8, x1: u8, y1: u8) {
        if x0 == x1 {
            // horizontal
            for y in y0..=y1 {
                self.display.write_pixel(x0.into(), y.into(), self.color);
            }
        } else if y0 == y1 {
            // vertical
            for x in x0..=x1 {
                self.display.write_pixel(x.into(), y0.into(), self.color);
            }
        } else {
            panic!("Line not straight: {} {} {} {}", x0, y0, x1, y1);
        }
    }

    pub fn draw_line(&mut self, x0: u8, y0: u8, x1: u8, y1: u8) {
        // Bresenham's line algorithm
        // https://en.wikipedia.org/wiki/Bresenham's_line_algorithm
        let mut x0: i16 = x0.into();
        let mut y0: i16 = y0.into();
        let x1: i16 = x1.into();
        let y1: i16 = y1.into();

        let dx = (x1 - x0).abs();
        let sx = if x0 < x1 { 1 } else { -1 };
        let dy = -(y1 - y0).abs();
        let sy = if y0 < y1 { 1 } else { -1 };
        let mut error = dx + dy;

        loop {
            self.the_math_checks_out_please_write_pixel(x0, y0);
            if x0 == x1 && y0 == y1 {
                return;
            };
            let e2 = 2 * error;
            if e2 >= dy {
                if x0 == x1 {
                    return;
                };
                error += dy;
                x0 += sx;
            }
            if e2 <= dx {
                if y0 == y1 {
                    return;
                };
                error += dx;
                y0 += sy;
            }
        }
    }

    #[allow(dead_code)]
    pub fn draw_line_aa(&mut self, x0: u8, y0: u8, x1: u8, y1: u8) {
        // DOES NOT WORK!
        // This function may work, but write_pixel_aa does not.
        let mut x0: i16 = x0.into();
        let mut y0: i16 = y0.into();
        let x1: i16 = x1.into();
        let y1: i16 = y1.into();

        let dx = (x1 - x0).abs();
        let sx = if x0 < x1 { 1 } else { -1 };
        let dy = (y1 - y0).abs();
        let sy = if y0 < y1 { 1 } else { -1 };
        let mut err = dx - dy;
        let mut e2;
        let mut x2;
        let mut y2;

        let ed: f32 = if dx + dy == 0 {
            1.0
        } else {
            ((dx * dx + dy * dy) as f32).sqrt()
        };
        let wd: f32 = (self.stroke + 1.0) / 2.0;

        loop {
            let alpha = (0.0).max(255.0 * (((err - dx + dy).abs() as f32) / ed - wd + 1.0));
            self.write_pixel_aa(x0, y0, alpha);
            e2 = err;
            x2 = x0;
            if 2 * e2 >= -dx {
                e2 += dy;
                y2 = y0;
                while (e2 as f32) < (ed * wd) && (y1 != y2 || dx > dy) {
                    y2 += sy;
                    let alpha = (0.0).max(255.0 * ((e2.abs() as f32) / ed - wd + 1.0));
                    self.write_pixel_aa(x0, y2, alpha);
                    e2 += dx;
                }
                if x0 == x1 {
                    break;
                }
                e2 = err;
                err -= dy;
                x0 += sx;
            }
            if 2 * e2 <= dy {
                e2 = dx - e2;
                while (e2 as f32) < ed * wd && (x1 != x2 || dx < dy) {
                    e2 += dy;
                    x2 += sx;
                    let alpha = (0.0).max(255.0 * ((e2.abs() as f32) / ed - wd + 1.0));
                    self.write_pixel_aa(x2, y0, alpha);
                }
                if y0 == y1 {
                    break;
                }
                err += dx;
                y0 += sy;
            }
        }
    }

    pub fn draw_rectangle(&mut self, x0: u8, y0: u8, x1: u8, y1: u8) {
        self.display
            .write_fill(x0.into(), y0.into(), x1.into(), y1.into(), self.color);
    }

    pub fn draw_circle(&mut self, x0: u8, y0: u8, radius: u8) {
        let mut x: i16 = radius.into();
        let mut y: i16 = 0;

        let x0: i16 = x0.into();
        let y0: i16 = y0.into();

        self.the_math_checks_out_please_write_pixel(x + x0, y + y0);
        self.the_math_checks_out_please_write_pixel(x + x0, -y + y0);
        self.the_math_checks_out_please_write_pixel(y + x0, x + y0);
        self.the_math_checks_out_please_write_pixel(-y + x0, x + y0);

        let mut p = 1 - x;
        while x > y {
            y += 1;

            if p <= 0 {
                p = p + 2 * y + 1;
            } else {
                x -= 1;
                p = p + 2 * y - 2 * x + 1;
            }

            if x < y {
                break;
            }

            self.the_math_checks_out_please_write_pixel(x + x0, y + y0);
            self.the_math_checks_out_please_write_pixel(-x + x0, y + y0);
            self.the_math_checks_out_please_write_pixel(x + x0, -y + y0);
            self.the_math_checks_out_please_write_pixel(-x + x0, -y + y0);

            if x != y {
                self.the_math_checks_out_please_write_pixel(y + x0, x + y0);
                self.the_math_checks_out_please_write_pixel(-y + x0, x + y0);
                self.the_math_checks_out_please_write_pixel(y + x0, -x + y0);
                self.the_math_checks_out_please_write_pixel(-y + x0, -x + y0);
            }
        }
    }

    pub fn draw_circle_filled(&mut self, x0: u8, y0: u8, radius: u8) {
        let mut x: i16 = radius.into();
        let mut y: i16 = 0;

        let x0: i16 = x0.into();
        let y0: i16 = y0.into();

        self.draw_straight_line(
            (-x + x0).try_into().unwrap(),
            (y + y0).try_into().unwrap(),
            (x + x0).try_into().unwrap(),
            (y + y0).try_into().unwrap(),
        );

        let mut p = 1 - x;
        while x > y {
            y += 1;

            if p <= 0 {
                p = p + 2 * y + 1;
            } else {
                x -= 1;
                p = p + 2 * y - 2 * x + 1;
            }

            if x < y {
                break;
            }

            self.draw_straight_line(
                (-x + x0).try_into().unwrap(),
                (y + y0).try_into().unwrap(),
                (x + x0).try_into().unwrap(),
                (y + y0).try_into().unwrap(),
            );
            self.draw_straight_line(
                (-x + x0).try_into().unwrap(),
                (-y + y0).try_into().unwrap(),
                (x + x0).try_into().unwrap(),
                (-y + y0).try_into().unwrap(),
            );

            if x != y {
                self.draw_straight_line(
                    (-y + x0).try_into().unwrap(),
                    (x + y0).try_into().unwrap(),
                    (y + x0).try_into().unwrap(),
                    (x + y0).try_into().unwrap(),
                );
                self.draw_straight_line(
                    (-y + x0).try_into().unwrap(),
                    (-x + y0).try_into().unwrap(),
                    (y + x0).try_into().unwrap(),
                    (-x + y0).try_into().unwrap(),
                );
            }
        }
    }

    pub fn draw_triangle(&mut self, x0: u8, y0: u8, x1: u8, y1: u8, x2: u8, y2: u8) {
        self.draw_line(x0, y0, x1, y1);
        self.draw_line(x1, y1, x2, y2);
        self.draw_line(x2, y2, x0, y0);
    }

    #[allow(non_snake_case)]
    pub fn draw_triangle_filled(&mut self, x0: u8, y0: u8, x1: u8, y1: u8, x2: u8, y2: u8) {
        // Sadly, doesn't work with points in arbitrary order yet
        // https://web.archive.org/web/20050408192410/http://sw-shader.sourceforge.net/rasterizer.html
        let Y1 = (16.0 * (y0 as f32)).round() as i32;
        let Y2 = (16.0 * (y1 as f32)).round() as i32;
        let Y3 = (16.0 * (y2 as f32)).round() as i32;

        let X1 = (16.0 * (x0 as f32)).round() as i32;
        let X2 = (16.0 * (x1 as f32)).round() as i32;
        let X3 = (16.0 * (x2 as f32)).round() as i32;

        // Deltas
        let dx12 = X1 - X2;
        let dx23 = X2 - X3;
        let dx31 = X3 - X1;

        let dy12 = Y1 - Y2;
        let dy23 = Y2 - Y3;
        let dy31 = Y3 - Y1;

        // Fixed-point deltas
        let fdx12 = dx12 << 4;
        let fdx23 = dx23 << 4;
        let fdx31 = dx31 << 4;

        let fdy12 = dy12 << 4;
        let fdy23 = dy23 << 4;
        let fdy31 = dy31 << 4;

        // Bounding rectangle
        let minx = (X1.min(X2).min(X3) + 0xF) >> 4;
        let maxx = (X1.max(X2).max(X3) + 0xF) >> 4;
        let miny = (Y1.min(Y2).min(Y3) + 0xF) >> 4;
        let maxy = (Y1.max(Y2).max(Y3) + 0xF) >> 4;

        // Half-edge constants
        let mut c1 = dy12 * X1 - dx12 * Y1;
        let mut c2 = dy23 * X2 - dx23 * Y2;
        let mut c3 = dy31 * X3 - dx31 * Y3;

        // Correct for fill convention
        if dy12 < 0 || (dy12 == 0 && dx12 > 0) { c1 += 1; }
        if dy23 < 0 || (dy23 == 0 && dx23 > 0) { c2 += 1; }
        if dy31 < 0 || (dy31 == 0 && dx31 > 0) { c3 += 1; }

        let mut cy1 = c1 + dx12 * (miny << 4) - dy12 * (minx << 4);
        let mut cy2 = c2 + dx23 * (miny << 4) - dy23 * (minx << 4);
        let mut cy3 = c3 + dx31 * (miny << 4) - dy31 * (minx << 4);

        for y in miny..maxy {
            let mut cx1 = cy1;
            let mut cx2 = cy2;
            let mut cx3 = cy3;

            for x in minx..maxx {
                if cx1 > 0 && cx2 > 0 && cx3 > 0 {
                    self.the_math_checks_out_please_write_pixel(x.try_into().unwrap(), y.try_into().unwrap());
                }

                cx1 = cx1 - fdy12;
                cx2 = cx2 - fdy23;
                cx3 = cx3 - fdy31;
            }

            cy1 = cy1 + fdx12;
            cy2 = cy2 + fdx23;
            cy3 = cy3 + fdx31;
        }
    }
}
