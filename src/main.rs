#![no_std]
#![no_main]

extern crate alloc;

use cortex_m_rt::entry;
use embedded_hal::digital::v2::OutputPin;
use hal::{
    gpio::Level,
    gpiote::Gpiote,
    pac::{twim0::frequency::FREQUENCY_A, CLOCK},
    prelude::_embedded_hal_blocking_delay_DelayMs as DelayMs,
    Clocks, Timer,
};
use nrf52832_hal as hal;
use panic_rtt_target as _;
use rtt_target::{rprintln, rtt_init_print};

use nrf52832_hal::twim;

mod drivers;
use drivers::{
    backlight::{Backlight, Brightness},
    button::Button,
    display::Display,
    rtc::RTCDriver,
};
mod font;
mod graphics;
use graphics::Graphics;
mod humantime;
use humantime::DatetimeProvider;
mod watchfaces;
use watchfaces::WatchfaceCarousel;
mod touch;
use touch::{Gesture, TouchEvent};

static BUILDTIME_CURRENT_TIME_STR: &str = env!("CURRENT_TIME");

#[entry]
fn main() -> ! {
    rtt_init_print!();
    rprintln!("> Booted");

    let p = hal::pac::Peripherals::take().unwrap();
    let port0 = hal::gpio::p0::Parts::new(p.P0);

    let mut delay = Timer::new(p.TIMER0);

    // CLOCK control
    let clocks = Clocks::new(p.CLOCK);
    let clocks = clocks.enable_ext_hfosc();
    // let clocks = clocks.disable_ext_hfosc();
    clocks.start_lfclk();

    let mut rtc_driver = RTCDriver::new(p.RTC0);
    rtc_driver.init();

    // button
    let mut button = Button::new(port0.p0_13, port0.p0_15);

    // Backlight ( should be moved to it's own driver )
    let mut backlight = Backlight::new(port0.p0_14, port0.p0_22, port0.p0_23);

    // display
    let mut display = Display::new(
        p.SPIM0,
        port0.p0_02,
        port0.p0_03,
        port0.p0_18,
        port0.p0_25,
        port0.p0_26,
    );
    display.init(&mut delay);

    // Enable interrupts
    let interrupt_pin = port0.p0_28.into_floating_input().degrade();
    let gpiote = Gpiote::new(p.GPIOTE);
    let channel_1 = gpiote.channel1();
    channel_1
        .input_pin(&interrupt_pin)
        .hi_to_lo()
        .enable_interrupt();

    let _touch_rst = port0.p0_10.into_push_pull_output(Level::High).degrade();
    let twim_pins = twim::Pins {
        scl: port0.p0_07.into_floating_input().degrade(),
        sda: port0.p0_06.into_floating_input().degrade(),
    };

    let mut twim = twim::Twim::new(p.TWIM1, twim_pins, FREQUENCY_A::K400);

    // the added seconds is an estimate of the time from embedding the var
    // until the watch is booted and loads it.
    let time_of_built: i64 = 6500 + BUILDTIME_CURRENT_TIME_STR.parse::<i64>().unwrap(); 
    let datetime = DatetimeProvider::new(rtc_driver, time_of_built);

    let mut graphics = Graphics::new(display);
    graphics.clear();

    // Super random graphics demo
    graphics.set_stroke(5.0);
    graphics.set_color(255, 0, 0);
    graphics.draw_line(4, 4, 80, 80);

    graphics.set_stroke(2.0);
    graphics.draw_line(140, 140, 80, 80);
    graphics.set_stroke(1.0);
    graphics.draw_line(230, 100, 10, 210);

    graphics.set_color(255, 255, 0);
    graphics.draw_rectangle(100, 160, 180, 215);

    graphics.set_stroke(1.0);
    graphics.set_color(255, 0, 255);
    graphics.draw_circle(70, 120, 50);

    graphics.set_color(200, 200, 200);
    graphics.draw_circle_filled(70, 160, 30);

    graphics.set_color(0, 0, 255);
    graphics.draw_circle_filled(160, 160, 40);
    graphics.set_color(50, 50, 50);
    graphics.draw_circle_filled(160, 160, 30);
    graphics.set_color(255, 255, 255);
    graphics.draw_circle_filled(160, 160, 10);
    graphics.draw_triangle(100, 20, 30, 180, 200, 60);
    graphics.set_color(100, 200, 100);
    graphics.draw_triangle_filled(100, 20, 30, 180, 200, 60);
    delay.delay_ms(100u16);

    let mut watchface_carousel = WatchfaceCarousel::new();
    let mut watchface = watchface_carousel.current();
    watchface.render_background(&mut graphics);
    watchface.render(&mut graphics, datetime.now());

    rprintln!("> Initialized");

    let mut buf = [0u8; 63];
    loop {
        // Button
        if button.pressed(&mut delay) {
            graphics.set_color(0, 0, 0);
            graphics.clear();
            graphics.set_stroke(3.0);

            graphics.set_color(255, 0, 0);
            graphics.draw_triangle_filled(0, 180, 74, 0, 0, 0);
            graphics.draw_triangle_filled(117, 115, 74, 0, 0, 180);

            graphics.set_color(255, 255, 255);
            graphics.draw_triangle_filled(117, 115, 45, 155, 90, 240);
            graphics.draw_triangle_filled(160, 240, 117, 115, 90, 240);
            graphics.draw_triangle_filled(143, 186, 160, 240, 240, 150);
            graphics.draw_triangle_filled(240, 150, 160, 240, 240, 240);

            graphics.set_color(255, 215, 0);
            graphics.draw_triangle_filled(97, 0, 74, 0, 142, 184);
            graphics.draw_triangle_filled(97, 0, 142, 184, 240, 137);
            graphics.draw_triangle_filled(240, 150, 240, 137, 142, 184);
            graphics.draw_triangle_filled(196, 94, 240, 137, 240, 50);

            graphics.set_color(255, 0, 0);
            graphics.draw_line(74, 0, 160, 240);
            graphics.set_color(0, 0, 0);
            graphics.draw_line(0, 180, 181, 79);
            graphics.draw_line(240, 135, 181, 79);
            graphics.draw_line(195, 93, 79, 220);
            graphics.set_color(255, 0, 0);
            graphics.draw_line(0, 240, 240, 150);

            loop {
                if button.pressed(&mut delay) {
                    watchface.render_background(&mut graphics);
                    break;
                }
            }
        } else {
            backlight.set(Brightness::LOW);
        }

        // Calibrate
        // SAFETY: lol. lmao.
        let mut legit_clock: CLOCK = unsafe { core::mem::transmute(()) };
        calibrate(&mut legit_clock);

        // Touch
        let result = channel_1.is_event_triggered();
        if result {
            channel_1.reset_events();
            twim.copy_write_then_read(0x15, &[0], &mut buf).unwrap();
            if let Some(event) = TouchEvent::parse(&buf) {
                match event {
                    TouchEvent {
                        x: _,
                        y: _,
                        gesture: Gesture::SlideRight,
                    } => {
                        watchface = watchface_carousel.prev();
                        watchface.render_background(&mut graphics);
                    }
                    TouchEvent {
                        x: _,
                        y: _,
                        gesture: Gesture::SlideLeft,
                    } => {
                        watchface = watchface_carousel.next();
                        watchface.render_background(&mut graphics);
                    }
                    _ => rprintln!("{:?}", event),
                }
            }
        }
        watchface.render(&mut graphics, datetime.now());
    }
}

fn calibrate(clock: &mut CLOCK) {
    // Takes around 2ms
    // clock.intenset.write(|w| unsafe { w.bits(1 << 3) });
    clock.intenclr.write(|w| unsafe { w.bits(0) });
    clock.tasks_cal.write(|w| unsafe { w.bits(1) });
    while clock.events_done.read().bits() == 0 {
    }
    clock.events_done.write(|w| unsafe { w.bits(0) });
}
