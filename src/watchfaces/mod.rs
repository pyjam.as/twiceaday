use alloc::boxed::Box;

use crate::humantime::NaiveDateTime;
use crate::Graphics;

mod analog;
mod pyjamas;
mod pyjamas_bg;
use analog::AnalogWatchface;
use pyjamas::PyjamasWatchface;

pub trait Watchface {
    fn render_background(&mut self, graphics: &mut Graphics);
    fn render(&mut self, graphics: &mut Graphics, now: NaiveDateTime);
}

pub struct WatchfaceCarousel {
    index: usize,
    watchfaces: [Box<dyn Watchface>; 2],
}

impl WatchfaceCarousel {
    pub fn new() -> Self {
        WatchfaceCarousel {
            index: 0,
            watchfaces: [
                Box::new(AnalogWatchface::new()),
                Box::new(PyjamasWatchface::new()),
            ],
        }
    }

    pub fn current(&mut self) -> &mut dyn Watchface {
        self.watchfaces[self.index].as_mut()
    }

    pub fn prev(&mut self) -> &mut dyn Watchface {
        self.index = if self.index == 0 {
            self.watchfaces.len() - 1
        } else {
            self.index - 1
        };
        self.current()
    }

    pub fn next(&mut self) -> &mut dyn Watchface {
        self.index = (self.index + 1) % self.watchfaces.len();
        self.current()
    }
}
